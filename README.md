# Description

This program brute-forces MD5 hashes to find the hidden message or password.
It supports multithreading and alphabet specification.

# Building

Requires
* gcc
* libssl-dev
* make
* cmake

```bash
cmake .
make md5
```

# Usage

```bash
md5 md5-hash <alphabet>

OPTIONS
    -j n    Uses n threads to accelerate brute forcing

    --help
    -h      Shows this help message
```

#include "catch.hpp"

#include "../src/word_iterator.h"

#include <cmath>
#include <string>

TEST_CASE("Going past word", "[word_iterator]")
{
    std::string alphabet = "ab";
    WordIterator iter(alphabet);
    SECTION("First word is not past last word")
    {
        REQUIRE( !iter.went_past_last_word() );
    }
}

TEST_CASE("next() = next(1)")
{
    std::string alphabet = "ab";
    WordIterator iter1(alphabet);
    WordIterator iter2(alphabet);
    iter1.next();
    iter2.next(1);
    std::string word1((const char*)iter1.get_word());
    std::string word2((const char*)iter2.get_word());
    REQUIRE(word1 == word2);
}

TEST_CASE("Advancing modifies the word", "[word_iterator]")
{
    std::string alphabet = "ab";
    WordIterator iter(alphabet);
    SECTION("Second word is not first word")
    {
        std::string first((const char *)iter.get_word());
        iter.next();
        std::string second((const char *)iter.get_word());
        REQUIRE( first != second );
    }
    SECTION("Third word is not second word")
    {
        iter.next();
        std::string second((const char *)iter.get_word());
        iter.next();
        std::string third((const char *)iter.get_word());
        REQUIRE( second != third );
    }
}

TEST_CASE("Advancing is linear", "[word_iterator]")
{
    std::string alphabet = "ab";
    WordIterator iter1(alphabet);
    WordIterator iter2(alphabet);
    SECTION("Same alphabet starts at same word")
    {
        std::string word1((const char*)iter1.get_word());
        std::string word2((const char*)iter2.get_word());
        REQUIRE( word1 == word2 );
    }
    SECTION("1 + 1 = 2")
    {
        INFO(iter1.get_word() << " | " << iter2.get_word());
        iter1.next(1);
        INFO(iter1.get_word() << " | " << iter2.get_word());
        iter1.next(1);
        INFO(iter1.get_word() << " | " << iter2.get_word());
        iter2.next(2);
        std::string word1((const char*)iter1.get_word());
        std::string word2((const char*)iter2.get_word());
        REQUIRE( word1 == word2 );
    }
    SECTION("3 + 4 = 7")
    {
        INFO(iter1.get_word() << " | " << iter2.get_word());
        iter1.next(3);
        INFO(iter1.get_word() << " | " << iter2.get_word());
        iter1.next(4);
        INFO(iter1.get_word() << " | " << iter2.get_word());
        iter2.next(7);
        std::string word1((const char*)iter1.get_word());
        std::string word2((const char*)iter2.get_word());
        REQUIRE( word1 == word2 );
    }
}

TEST_CASE("Harcorded test values", "[word_iterator]")
{
    std::string alphabet = "ab";
    WordIterator iter(alphabet);
    SECTION("first is a")
    {
        std::string hardcoded = "a";
        std::string word((const char*)iter.get_word());
        REQUIRE(word == hardcoded);
    }
    SECTION("second is b")
    {
        iter.next(1);
        std::string hardcoded = "b";
        std::string word((const char*)iter.get_word());
        REQUIRE(word == hardcoded);
    }
    SECTION("third is aa")
    {
        iter.next(2);
        std::string hardcoded = "aa";
        std::string word((const char*)iter.get_word());
        REQUIRE(word == hardcoded);
    }
    SECTION("fourth is ab")
    {
        iter.next(3);
        std::string hardcoded = "ab";
        std::string word((const char*)iter.get_word());
        REQUIRE(word == hardcoded);
    }
    SECTION("fifth is ba")
    {
        iter.next(4);
        std::string hardcoded = "ba";
        std::string word((const char*)iter.get_word());
        REQUIRE(word == hardcoded);
    }
}

TEST_CASE("Word length double letter alphabet", "[word_iterator]")
{
    std::string alphabet = "ab";
    WordIterator iter(alphabet);
    SECTION("First word is of length 1")
    {
        REQUIRE( iter.get_word_length() == 1 );
    }
    SECTION("Second word is of length 1")
    {
        iter.next(1);
        REQUIRE( iter.get_word_length() == 1 );
    }
    SECTION("Third word is of length 2")
    {
        iter.next(2);
        INFO("Current word is " << iter.get_word() );
        REQUIRE( iter.get_word_length() == 2 );
    }
}

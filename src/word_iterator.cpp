#include "word_iterator.h"

#include <iostream>

/*
 * PRIVATE
 */

void WordIterator::index_add(unsigned int n, unsigned int position)
{
    // If we're going past the word length, update it
    if(word_length < position)
    {
        word_length = position;
        n -= 1;
    }
    // Compute new values
    unsigned int sum = n + (unsigned int)index[position];
    unsigned char value = (unsigned char)(sum % ab_length);
    unsigned int left = sum / ab_length;
#if 0
    std::cout << "n " << n << ", pos " << position << " (sum " << sum
        << ", value " << +value << ", left " << left << ")" << std::endl;
#endif
    index[position] = value;
    // If we reached the last byte of index, don't go further
    if(position >= max_word_length - 1)
    {
        // If the index value went higher than possible values, we went past
        // the last word
        if(left > 0)
            went_past_last = true;
        return;
    }
    // else, if there is a 'carry', propagate it further
    else if(left > 0)
    {
        index_add(left, position + 1);
    }
}

/*
 * PUBLIC
 */

void WordIterator::next(unsigned int steps)
{
    if(steps > 0)
        index_add(steps);
}

const unsigned char * WordIterator::get_word()
{
    // Update the word with index values
    for(unsigned int pos = 0; pos < word_length; pos++)
        word[pos] = alphabet[index[word_length - pos]];
    return word;
}

#pragma once

#include "word_iterator.h"

#include <cstring>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

/** This class brutes forces a MD5 hash with thread parallelization */
class BruteForcer
{
    private:
        const unsigned char * const hash; ///< The hash to crack
        const std::string &alphabet; ///< The alphabet to make passwords from
        const unsigned int thread_count; ///< The number of threads to use

        unsigned char password[max_word_length + 1]; ///< The resulting password
        bool found = false; ///< True when the password has been found

        std::vector<WordIterator> iterators; ///< The iterators for each thread
        std::vector<std::thread> threads; ///< The threads used

        /** This function is a process brute-forcing the hash
         * \param thread_id The id of the thread running this function */
        void brute_force(const unsigned int thread_id);

    public:
        /** The constructor */
        BruteForcer(const unsigned char * const hash,
                    const std::string & alphabet,
                    unsigned int thread_count = 1)
            : hash(hash), alphabet(alphabet), thread_count(thread_count)
        {
            memset(password, 0, max_word_length + 1);
            for(unsigned int thread = 0; thread < thread_count; thread++)
            {
                iterators.push_back(WordIterator(alphabet));
                threads.push_back(
                        std::thread(&BruteForcer::brute_force, this, thread));
            }
            for(unsigned int thread = 0; thread < thread_count; thread++)
                threads[thread].join();
        }

        /** This returns whether the password was found
         * \return true if found, false otherwise */
        bool has_found() const { return found; }

        /** This getter returns the password
         * \return the password */
        const unsigned char * get_password() const { return password; }
};

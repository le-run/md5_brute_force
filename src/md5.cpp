#include <cstring>
#include <iostream>
#include <openssl/md5.h>

#include "brute_forcer.h"

std::string alphabet("abcdefghijklmnopqrstuvwxyz0123456789");
unsigned int ab_length = alphabet.length();

unsigned char target[MD5_DIGEST_LENGTH];

void output_digest(const unsigned char * const digest)
{
    std::cout << std::hex;
    for(unsigned int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        unsigned char value = digest[i];
        if(value < 16)
            std::cout << "0";
        std::cout << +value;
    }
    std::cout << std::dec;
}

/** Reads a digest from a string */
void parse_input(const char * source)
{
    const std::string s(source);
    for(unsigned int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        std::string hex = s.substr(2*i,2);
        char value = (char)(int)strtol(hex.c_str(), NULL, 16);
        target[i] = value;
    }
}

void print_usage(const char * const executable_name)
{
    std::cerr << "Usage: " << executable_name << " md5-hash <alphabet>"
        << std::endl << "OPTIONS" << std::endl
        << "\t-j n\tUses n threads to accelerate brute forcing." << std::endl
        << "\t--help" << std::endl
        << "\t-h\tShows this help message" << std::endl;
}

#ifndef __TEST__
int main(int argc, char * argv[])
{
    unsigned int thread_count = 1;
    unsigned char position = 0;
    for(unsigned int argi = 1; argi < (unsigned int)argc; argi++)
    {
        std::string argument(argv[argi]);
        if(argument == "-j")
        {
            argi++;
            if(argi == (unsigned int)argc)
            {
                std::cerr << "Option -j requires an argument." << std::endl;
                print_usage(argv[0]);
                return -1;
            }
            thread_count = atoi(argv[argi]);
        }
        else if(argument == "-h" || argument == "--help")
        {
            print_usage(argv[0]);
            return 0;
        }
        else if(position == 0 && argument[0] != '-')
        {
            unsigned int hash_length = strlen(argv[argi]);
            if(hash_length < MD5_DIGEST_LENGTH * 2)
            {
                std::cerr << "MD5 hash should be " << MD5_DIGEST_LENGTH * 2
                    << " letters long and not " << hash_length
                    << std::endl;
                return -1;
            }
            parse_input(argv[1]);
            position++;
        }
        else if(position == 1 && argument[0] != '-')
        {
            alphabet = std::string(argv[argi]);
            ab_length = alphabet.length();
        }
        else
        {
            std::cerr << "Unknown argument " << argv[argi] << std::endl;
            print_usage(argv[0]);
            return -1;
        }
    }
    if(position == 0)
    {
        print_usage(argv[0]);
        return -1;
    }
    std::cout << "Starting brute force on ";
    output_digest(target);
    std::cout << " with " << thread_count
        << (thread_count == 1 ? " thread" : " threads")
        << " and alphabet " << alphabet << std::endl;
    BruteForcer bf(target, alphabet, thread_count);
    if(bf.has_found())
        std::cout << "Password found : " << bf.get_password() << std::endl;
    else
        std::cout << "Password not found" << std::endl;
    return 0;
}
#endif

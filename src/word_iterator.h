#pragma once

#include <cstring>
#include <string>

const unsigned int max_word_length = 32;

/** \brief This class represents an iterator over words of a given alphabet.
 *
 * WordIterator permits the iteration over words from alphabet* (Klein star,
 * not pointer notation) up to a maximum length. THIS CLASS DOES NOT SUPPORT
 * ALPHABETS OF LESS THAN 2 LETTERS. */
class WordIterator
{
    private:
        const std::string & alphabet; ///< The alphabet over which words a made
        const unsigned int ab_length; ///< The length of the alphabet
        unsigned int word_length = 1; ///< The length of the current word
        unsigned char index[max_word_length]; /**< The index of the current
                                               * word in byte little endian */
        unsigned char word[max_word_length + 1]; ///< The current word
        bool went_past_last = false; /**< Did index loop over and went past
                                      * the last word ? */

        /** This function takes care of adding n to the max_word_length-sized
         * integer that index represents
         * \param n The number to add to index
         * \param position The position in the table at which n should be added
         */
        void index_add(unsigned int n, unsigned int position = 1);
    public:
        /** Constructor
         * \param alphabet The alphabet from which words are made */
        WordIterator(const std::string & alphabet)
            : alphabet(alphabet), ab_length(alphabet.length())
        {
            memset(word, 0, max_word_length + 1);
            memset(index, 0, max_word_length);
        }

        /** This function returns whether the iterator went past the last word
         * \return true if the iterator went past the las word, false otherwise
         */
        bool went_past_last_word() const { return went_past_last; }

        /** This function advances the iterator.
         * \param steps The number of steps to advance */
        void next(unsigned int steps = 1);

        /** This getter returns the current word length
         * \return the current word length */
        unsigned int get_word_length() const { return word_length; }

        /** This getter returns a pointer to the word the iterator is at.
         * \return A pointer to the current word */
        const unsigned char * get_word();
};

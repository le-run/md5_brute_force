#include "brute_forcer.h"

#include <openssl/md5.h>

/*
 * PRIVATE
 */

void BruteForcer::brute_force(const unsigned int thread_id)
{
    unsigned char digest[MD5_DIGEST_LENGTH];
    WordIterator * iter = &iterators[thread_id];
    // Start at the thread_idth word
    iter->next(thread_id);
    while(!found && !iter->went_past_last_word())
    {
        MD5(iter->get_word(), iter->get_word_length(), digest);
        if(!memcmp(hash, digest, MD5_DIGEST_LENGTH))
        {
            strcpy((char*)password, (char*)iter->get_word());
            found = true;
            return;
        }
        iter->next(thread_count);
    }
}

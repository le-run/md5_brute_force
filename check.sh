#!/bin/bash
# -*- coding: UTF8 -*-

CPPC_FLAGS="-q --std=c++11 --enable=all src/*.cpp src/*.h"
echo "--- CHECKING CONFIGURATION ---"
cppcheck --debug-warnings --check-config $CPPC_FLAGS
echo "--- CHECKING CODE ---"
cppcheck $CPPC_FLAGS
